# Warning: logic is based on the fact that openings / appointments are contained withina  day
# Warning: logic is based on the fact that openings do not overlap
class Programmer

  attr_reader :start_time, :openings, :appointments

  PERIOD = 7.days
  STEP = 30.minutes

  def initialize(start_time)
    @start_time = start_time
    @openings = get_openings
    @appointments = get_appointments
  end

  def availabilities
    format_slots available_slots
  end

  def format_slots(available_slots)
    # [{"date":"2014/08/04","slots":["12:00","13:30"]},{"date":"2014/08/05","slots":["09:00", "09:30"]},
    res = {}
    0.upto(6).each do |day_offset|
      date = (start_time + day_offset.days).to_date
      res[date] = []
    end

    available_slots.each do |slot|
      # date_s = slot.strftime("%Y/%m/%d")
      date_s = slot.to_date
      time_s = slot.strftime("%H:%M").gsub(/^0/, "")
      res[date_s] << time_s
    end

    res.map{|k, v| {:date => k, :slots => v} }
  end

  def available_slots
    slots.select do |slot|
      conflicting_appointments = appointments.any? { |appointment| slot_overlaps?(appointment, slot) }
      inside_opening = openings.any? { |opening| slot_inside?(opening, slot) }

      inside_opening && !conflicting_appointments
    end
  end

  def slots()
    0.upto(7 * 24 * 2).map do |offset|
      # WARNING: start_time should be at the beginning of day
      start_time + (offset * 30).minutes
    end
  end

  def transform_to_current_week(datetime)
    wday = datetime.wday
    # it may not start monday
    to_add = (wday + 7 - start_day) % 7

    start_time + to_add.days + datetime.hour.hours + datetime.min.minutes + datetime.sec.seconds
  end

  def get_openings
    events_for_current_week(Event.openings)
  end

  def get_appointments
    events_for_current_week(Event.appointments)
  end

  # returns array with 2-tuples
  def events_for_current_week(query)
    punctual_openings = query.events_between(start_time, end_time).non_recurring.map do |event|
      [event.starts_at, event.ends_at]
    end
    recurring_openings = query.recurring.map do |recurring_opening|
      [transform_to_current_week(recurring_opening.starts_at),
       transform_to_current_week(recurring_opening.ends_at)]
    end

    punctual_openings + recurring_openings
  end

  def slot_inside?(event, slot)
    event_start, event_end = event
    event_start <= slot && event_end >= slot &&
        event_start <= (slot + STEP) && event_end >= (slot + STEP)
  end

  def slot_overlaps?(event, slot)
    event_start, event_end = event
    # start of event is inside slot
    return true if slot < event_start && event_start < (slot + STEP)
    # end of event is inside slot
    return true if slot < event_end && event_end < (slot + STEP)

    # or includes completely
    return true if event_start <= slot && (slot + STEP) <= event_end

    false
  end

  private

  def start_day
    start_time.wday
  end

  def end_time
    start_time + PERIOD
  end

end