class Event < ActiveRecord::Base

  class << self

    def availabilities(start_time)
      # FIXME: make proper exception classes
      raise Exception.new("Not date") unless start_time.is_a?(DateTime)
      raise Exception.new("Not beginning of day") unless start_time == start_time.beginning_of_day
      # Locale maybe?
      raise Exception.new("Not beginning of day") unless start_time.wday == 0

      ::Programmer.new(start_time).availabilities
    end

    def events_between(start_time, end_time)
      where(["starts_at >= ? AND ends_at <= ?", start_time, end_time])
    end

    def recurring
      where(weekly_recurring: true)
    end

    def non_recurring
      where(weekly_recurring: [nil, false])
    end

    def openings
      where(kind: 'opening')
    end

    def appointments
      where(kind: 'appointment')
    end
  end

end
