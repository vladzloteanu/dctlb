# test/models/event_test.rb

require 'test_helper'

class EventTest < ActiveSupport::TestCase

  test "one simple test example" do
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 12:30"), weekly_recurring: true
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-11 11:30")

    _tst_slots(
        0 => [],
        1 => ["9:30", "10:00", "11:30", "12:00"],)
  end


  test "no availabilities" do
    _tst_slots(
        0 => [],
        1 => [],)
  end

  test "invalid data - not DateTime" do
    assert_raises(Exception) do
      Event.availabilities "2014-08-10"
    end
  end


  test "invalid data - not beginning of day" do
    assert_raises(Exception) do
      Event.availabilities DateTime.parse("2014-08-10 10:00")
    end
  end

  test "invalid data - not beginning of week" do
    assert_raises(Exception) do
      Event.availabilities DateTime.parse("2014-08-11")
    end
  end

  test "overlapping openings" do
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 12:30"), weekly_recurring: true
    # overlaps with a non-recurrent one
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-11 10:00"), ends_at: DateTime.parse("2014-08-11 13:30"), weekly_recurring: true
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-11 11:30")

    # A new slot from new opening
    _tst_slots(
        0 => [],
        1 => ["9:30", "10:00", "11:30", "12:00", "12:30", "13:00"],)
  end

  test "overlapping appointments" do
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 12:30"), weekly_recurring: true
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-11 11:30")
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 9:30"), ends_at: DateTime.parse("2014-08-11 12:00")

    _tst_slots(
        0 => [],
        1 => ["12:00"],)
  end

  test "non-recurring oppenings in different week" do
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 12:30")
    # with appointment event if there is no oppening
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-11 11:30")

    _tst_slots(
        0 => [],
        1 => [],)
  end

  test "non-recurring appointments in different week" do
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 12:30"), weekly_recurring: true
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-30 11:30")

    _tst_slots(
        0 => [],
        1 => ["9:30", "10:00", "10:30", "11:00", "11:30", "12:00"],)
  end

  def _tst_slots(dict)
    availabilities = Event.availabilities DateTime.parse("2014-08-10")

    assert_equal Date.new(2014, 8, 16), availabilities[6][:date]
    assert_equal Date.new(2014, 8, 11), availabilities[1][:date]

    assert_equal 7, availabilities.length

    dict.each do |idx, slots|
      assert_equal slots, availabilities[idx][:slots]
    end
  end

end